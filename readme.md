# Sistemi Operativi AA 2018/19
Materiale didattico per il corso di Sistemi Operativi dell'Anno Accademico 2018/2019 - Sapienza Universita' di Roma.

Questa repository contiene:
- slides del corso
- sorgenti ed esericizi
- progetti
- esami e risultati
per l'anno accademico 2018/19.

### Prerequisiti
1. compilatore C
```bash
sudo apt-get install build-essential
```
2. git
```bash
sudo apt-get install git
```

### Cosa fare con questa repository?
0. Apri il terminale
1. Clona la repository sul tuo computer
```bash
    cd <somewhere_on_your_pc>
    git clone https://gitlab.com/grisetti/sistemi_operativi_2018_19
```
2. Periodicamente aggiorna la repository per sincronizzare i nuovi contenuti
```bash
    cd <repo_directory>
    git pull
```

E' bene che la repository sia **sempre aggiornata** :)
